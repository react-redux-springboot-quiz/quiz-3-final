const initial = {
    notes: null
};

export const noteReducer = (state = initial, action) => {
    switch (action.type) {
        case "GET_ALL_NOTES":
            return {
                ...state,
                notes: action.payload
            };
        default:
            return state;
    }
};