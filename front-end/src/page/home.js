import React, {Component} from 'react';
import {getAllNotes} from "../action/getAllNotes";
import {connect} from "react-redux";

class Home extends Component {


    constructor(props, context) {
        super(props, context);
    }

    componentDidMount() {
        this.props.getAllNotes();
    }

    render() {
        return (
            <div>
                {this.props.notes}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    notes: state.noteReducer.notes
});

const mapDispatchToProps = (dispatch) => ({
    getAllNotes: () => {
        dispatch(getAllNotes())
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);