package com.twuc.webApp;

import com.twuc.webApp.contract.OrderRequest;
import com.twuc.webApp.contract.AddProductRequest;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

class ProductOrderTest extends ApiTestBase {

    @Test
    void create_table() {

    }

    @Test
    void should_insert_new_order_list_when_there_is_no_same_product() throws Exception {
        addProduct();

        mockMvc.perform(post("/api/order/add")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(serialize(new OrderRequest(1L))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.amount").value(1));
    }

    @Test
    void should_add_1_to_amount() throws Exception {
        addProduct();
        addOrder();

        mockMvc.perform(post("/api/order/add")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(serialize(new OrderRequest(1L))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.amount").value(2));
    }

    @Test
    void should_add_2_to_amount() throws Exception {
        addProduct();
        addOrder();
        addOrder();

        mockMvc.perform(post("/api/order/add")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(serialize(new OrderRequest(1L))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.amount").value(3));
    }

    // TODO no different???
    @Test
    void should_get_order_list_when_exist() throws Exception {

        addProduct();
        addOrder();
        String expected = "{id=1, amount=1, product={id=1, name=sprite, price=1.0, unit=bottle, picURL=www}}";

        mockMvc.perform(get("/api/orders")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.productOrderList").value(expected));
    }

    @Test
    void should_give_wrong_message_when_the_order_is_empty() throws Exception {
        mockMvc.perform(get("/api/orders")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.tips").value("暂无订单，返回商城页面继续购买"));
    }

    @Test
    void should_delete_order_by_product_id() throws Exception {
        addProduct();
        addOrder();

        mockMvc.perform(post("/api/order/delete")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(new OrderRequest(1L))))
                .andExpect(status().isOk());
    }

    private void addOrder() throws Exception {
        mockMvc.perform(post("/api/order/add")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(serialize(new OrderRequest(1L))));
    }


    private void addProduct() throws Exception {
        mockMvc.perform(post("/api/product/add")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(serialize(new AddProductRequest("sprite", (float) 1.0, "bottle", "www"))));
    }
}
