package com.twuc.webApp;

import com.twuc.webApp.contract.AddProductRequest;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

class ProductTest extends ApiTestBase {
    @Test
    void create() {
    }

    @Test
    void should_save_new_product() throws Exception {
        mockMvc.perform(post("/api/product/add")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(serialize(new AddProductRequest("", (float) 1.0, "", ""))))
                .andExpect(status().isOk());
    }

    @Test
    void should_get_wrong_message_when_add_the_same_name_product() throws Exception {
        addProduct();
        mockMvc.perform(post("/api/product/add")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(serialize(new AddProductRequest("sprite", (float) 1.0, "", ""))))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.tips").value("商品名称已存在，请输入新的商品名称"));
    }

    private void addProduct() throws Exception {
        mockMvc.perform(post("/api/product/add")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(serialize(new AddProductRequest("sprite", (float) 1.0, "bottle", "www"))));
    }

    @Test
    void should_return_all_products() throws Exception {
        addProduct();
        addProduct();

        mockMvc.perform(get("/api/products"))
                .andExpect(status().isOk())
                .andExpect(content().string("[{\"id\":1,\"name\":\"sprite\",\"price\":1.0,\"unit\":\"bottle\",\"picURL\":\"www\"}]"));
    }
}
