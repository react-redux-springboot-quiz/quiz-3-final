create table product
(
    id bigint primary key auto_increment,
    name varchar(256) not null ,
    price float not null ,
    unit varchar(256) not null ,
    picURL varchar(256) not null
);