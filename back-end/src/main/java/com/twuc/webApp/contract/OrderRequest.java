package com.twuc.webApp.contract;

public class OrderRequest {
    private Long product_id;

    public OrderRequest() {
    }

    public OrderRequest(Long product_id) {
        this.product_id = product_id;
    }

    public Long getProduct_id() {
        return product_id;
    }
}
