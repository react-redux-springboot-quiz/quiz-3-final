package com.twuc.webApp.contract;

import javax.validation.constraints.NotNull;

public class AddProductRequest {

    @NotNull
    private String name;

    @NotNull
    private Float price;

    @NotNull
    private String unit;

    @NotNull
    private String picURL;

    public AddProductRequest() {
    }

    public AddProductRequest(String name, Float price, String unit, String picURL) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.picURL = picURL;
    }

    public String getName() {
        return name;
    }

    public Float getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getPicURL() {
        return picURL;
    }
}
