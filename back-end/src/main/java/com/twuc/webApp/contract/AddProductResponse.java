package com.twuc.webApp.contract;

public class AddProductResponse {
    private String tips;

    public AddProductResponse(String tips) {
        this.tips = tips;
    }

    public String getTips() {
        return tips;
    }
}
