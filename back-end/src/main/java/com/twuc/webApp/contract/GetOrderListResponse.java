package com.twuc.webApp.contract;

import com.twuc.webApp.entity.ProductOrder;

import java.util.List;

public class GetOrderListResponse {
    private String tips = "暂无订单，返回商城页面继续购买";
    private List<ProductOrder> productOrderList;

    public GetOrderListResponse(List<ProductOrder> productOrderList) {
        this.productOrderList = productOrderList;
    }

    public String getTips() {
        return tips;
    }

    public List<ProductOrder> getProductOrderList() {
        return productOrderList;
    }
}
