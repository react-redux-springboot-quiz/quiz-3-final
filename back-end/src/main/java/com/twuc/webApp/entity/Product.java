package com.twuc.webApp.entity;

import javax.persistence.*;

@Entity
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private Float price;

    @Column(nullable = false)
    private String unit;

    @Column(nullable = false)
    private String picURL;

    public Product() {
    }

    public Product(String name, Float price, String unit, String picURL) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.picURL = picURL;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Float getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getPicURL() {
        return picURL;
    }

}