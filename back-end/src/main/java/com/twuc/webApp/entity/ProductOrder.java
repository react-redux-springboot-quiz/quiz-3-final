package com.twuc.webApp.entity;

import javax.persistence.*;

@Entity
public class ProductOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column()
    private int amount;

    @OneToOne
    private Product product;

    public ProductOrder(int amount) {
        this.amount = amount;
    }

    public ProductOrder() {
    }

    public Long getId() {
        return id;
    }

    public int getAmount() {
        return amount;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public void addAmount(){
        amount ++;
    }
}
