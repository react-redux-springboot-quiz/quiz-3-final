package com.twuc.webApp.controller;

import com.twuc.webApp.contract.OrderRequest;
import com.twuc.webApp.contract.GetOrderListResponse;
import com.twuc.webApp.entity.ProductOrder;
import com.twuc.webApp.entity.OrderRepository;
import com.twuc.webApp.entity.Product;
import com.twuc.webApp.entity.ProductsRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class ProductOrderController {

    private ProductsRepository productsRepository;
    private OrderRepository orderRepository;

    public ProductOrderController(ProductsRepository productsRepository, OrderRepository orderRepository) {
        this.productsRepository = productsRepository;
        this.orderRepository = orderRepository;
    }

    // TODO 请求同一个id
    @PostMapping("/order/add")
    ResponseEntity<ProductOrder> addOrder(@RequestBody OrderRequest orderRequest) {
        Product productFound = productsRepository.findById(orderRequest.getProduct_id()).get();
        Optional<List<ProductOrder>> orderFound = orderRepository.findByProduct_Id(orderRequest.getProduct_id());

        if (orderFound.isPresent()) {
            orderFound.get().get(0).addAmount();
            orderRepository.save(orderFound.get().get(0));
            orderRepository.flush();
        } else {
            ProductOrder productOrder = new ProductOrder(1);
            productOrder.setProduct(productFound);
            orderRepository.save(productOrder);
        }

        Optional<List<ProductOrder>> orderFoundAfterAdd = orderRepository.findByProduct_Id(orderRequest.getProduct_id());

        return ResponseEntity.status(HttpStatus.OK).body(orderFoundAfterAdd.get().get(0));
    }

    @GetMapping("/orders")
    ResponseEntity<GetOrderListResponse> getOrders() {
        GetOrderListResponse getOrderListResponse = new GetOrderListResponse(orderRepository.findAll());
        return ResponseEntity.status(HttpStatus.OK).body(getOrderListResponse);
    }


    @PostMapping("/order/delete")
    ResponseEntity deleteOrder(@RequestBody OrderRequest orderRequest){
        orderRepository.delete(orderRepository.findByProduct_Id(orderRequest.getProduct_id()).get().get(0));
        orderRepository.flush();
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
