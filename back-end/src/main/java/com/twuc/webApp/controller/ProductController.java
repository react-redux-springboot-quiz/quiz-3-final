package com.twuc.webApp.controller;

import com.twuc.webApp.contract.AddProductRequest;
import com.twuc.webApp.contract.AddProductResponse;
import com.twuc.webApp.contract.OrderRequest;
import com.twuc.webApp.entity.Product;
import com.twuc.webApp.entity.ProductsRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(value = {"*"})
@RequestMapping("/api")
public class ProductController {

    private ProductsRepository productsRepository;

    public ProductController(ProductsRepository productsRepository) {
        this.productsRepository = productsRepository;
    }

    @PostMapping("/product/add")
    ResponseEntity<AddProductResponse> addProduct(@RequestBody @Valid AddProductRequest addProductRequest) {
        Product product =
                new Product(addProductRequest.getName(), addProductRequest.getPrice(), addProductRequest.getUnit(), addProductRequest.getPicURL());
        if (productsRepository.findAllByName(addProductRequest.getName()).isPresent()){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).contentType(MediaType.APPLICATION_JSON_UTF8)
                    .body(new AddProductResponse("商品名称已存在，请输入新的商品名称"));
        }

        productsRepository.save(product);
        return ResponseEntity.status(HttpStatus.OK)
                .header("Access-Control-Expose-Headers", "Location")
                .build();
    }

    @GetMapping("/products")
    ResponseEntity<List<Product>> getAllProducts(){
        List<Product> allProducts = productsRepository.findAll();
        return ResponseEntity.status(HttpStatus.OK).body(allProducts);
    }
}
